"use strict";
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var CordovaApp = /** @class */ (function () {
    function CordovaApp() {
        console.log('>>> app.constructor()');
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.getElementById('doStuffButton').addEventListener('click', this.onDoStuffButtonClick);
    }
    CordovaApp.prototype.onDeviceReady = function () {
        console.log('app.onDeviceReady()');
        this.receivedEvent('deviceready');
    };
    CordovaApp.prototype.onDoStuffButtonClick = function () {
        console.log('>>> doing stuff!');
        alert('Stuff done!');
    };
    CordovaApp.prototype.receivedEvent = function (id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');
        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');
        console.log('Received Event: ' + id);
    };
    return CordovaApp;
}());
exports.CordovaApp = CordovaApp;
console.log('>>> index.js code');
var instance = new CordovaApp();
//# sourceMappingURL=index.js.map