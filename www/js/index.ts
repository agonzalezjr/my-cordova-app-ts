/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

export class CordovaApp {
    constructor() {
        console.log('>>> app.constructor()');
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.getElementById('doStuffButton').addEventListener('click', this.onDoStuffButtonClick);
    }

    onDeviceReady() {
        console.log('app.onDeviceReady()');
        // this.receivedEvent('deviceready');
        const id = 'deviceready';

        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }

    onDoStuffButtonClick() {
        console.log('>>> doing stuff!');
        alert('Stuff done in TypeScript!');
    }

    receivedEvent(id: string) {
    }
}

console.log('>>> index.ts code');
let instance = new CordovaApp();